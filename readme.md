#Proposal for a C++ channel class

###I. Introduction

This proposal is for addition of a channel class to the `std` namespace that represents a similar mechanism found in other programming languages (e.g. Go, Limbo, and occam). The channel concept can be traced to the Communicating Sequential Processes (CSP) [1] paper and has proven useful in constructing provably correct concurrent applications. This proposal contains two channel types - blocking and buffered (non-blocking) channels.


###II. Motivation

The motivation for this proposal is based on the increased inclusion of basic synchronization primitives (i.e. `std::mutex`, `std::condition_variable`). The proposed channel class should be considered a synchronization primitive that would provide safe and robust inter-thread communication and data sharing. At present, inter-thread communication and data sharing require non-trivial usage of multiple synchronization primitives in order to achieve the desirable performance increase possible in a multi-threaded design. Channels would also remove common mistakes made when attempting to leverage a multi-threaded design. Users of this primitive would run the gamut from novice to expert in any field with applications benefiting from a multi-threaded design.

As previously mentioned, inter-thread communication and data sharing often requires multiple synchronization primitives that must be utilized in order to avoid multi-threaded bugs (e.g. data race and starvation) as well as encode potentially complex read/write coordination. Below are two examples of the producer consumer problem - the first utilizing currently existing synchronization primitives and the second using the proposed channel class.

####Produce/Consumer solution using current synchronization primitives

    // Shared state
    std::mutex data_lock;
    std::condition_variable data_available;
    std::queue<int> data;

    // Producer thread
    for (auto i = 1; i <= 48; ++i) {
        // Complex logic to create data

        std::unique_lock<std::mutex> lock{ data_lock };
        data.push(i);
        lock.unlock();
        data_available.notify_all();
    }
    data.push(-1);

    // Consumer thread
    std::unique_lock<std::mutex> lock{ data_lock };
    data_available.wait(lock, [&] { return !data.empty(); });
    auto r = data.front();
    data.pop();
    lock.unlock();
    while (r != -1) {
        // Complex logic to process data
        std::cout << r << "\\n";

        lock.lock();
        data_available.wait(lock, [&] { return !data.empty(); });
        r = data.front();
        data.pop();
        lock.unlock();
    }

In the above example notice the explicit decision that must be made in order to properly notify the consumer new data is available. In this trivial example the notification is straight forward but in a more complex example, when data production is rapid, a decision must be made on frequency of notification which can complicate locking. The consumer thread also has a non-trivial decision that is demonstrated in this example. Once data is available the consumer thread should extract it and release locks as quickly as possible to enable the producer thread to make progress on production. In both the producer and consumer the developer is burdened with non-trivial decisions that in a majority of cases aren't interesting to the actual production or consumption algorithm. Let us now consider the approach with a channel.

####Produce/Consumer solution using proposed channel primitive

    // Shared state
    channel<int> ch;

    // Producer thread
    for (auto i = 1; i <= 48; ++i) {
        // Complex logic to create data
        ch << i;
    }
    ch << -1;

    // Consumer thread
    int r;
    ch >> r;
    while (r != -1) {
        // Complex logic to process data
        std::cout << r << "\\n";
        ch >> r;
    }

In this example the developer is free to focus on the actual algorithms of production and consumption rather than the synchronization of the two concurrent threads.

A reference implementation of the channel class and example of usage can be found at: [https://bitbucket.org/linuxuser27/c-channel](https://bitbucket.org/linuxuser27/c-channel).


###III. Impact On the Standard

The proposal channel implementation would not require any additional support other than what is currently included in a C++11 compliant compiler and `std` library. This does not alter any existing library or language feature, but would add the `channel` type to the `std` namespace.


###IV. Technical Specifications

The proposed channel class has the following API surface:

    template <typename T>
    class channel
    {
    public:
        // Determine the number of elements the channel can contain without blocking
        int capacity() const;

        // Check if the channel is closed
        bool is_closed() const;

        // Close the channel
        void close();

        // Insert an element into the channel
        // Type T must support std::move()
        bool insert(T &t);

        // Insert an element into the channel
        channel & operator<<(T t);

        // Extract an element from the channel
        // channel_entry is a tuple of type <T, bool> where bool indicates if the channel is closed.
        channel_entry extract();

        // Extract an element from the channel
        channel & operator>>(T &t);

        // Iterators to support Range-based for loop
        iterator begin();
        iterator end();
    };

The API surface is designed to align with the Go language channel primitive [2]. This decision was made to ease potential adoption since Go is the most popular language utilizing channels according to [TIOBE](http://www.tiobe.com/tiobe-index/). It should be noted the use of passing the value of an element for the `channel & operator<<(T t)` function. This decision was made in order to allow the use of numerical constants.

The exposed API on the `channel<T>` class is a generalized surface for the channel concept, but it by no means limits the synchronization primitive being proposed. The `channel<T>` class acts as a proxy for the underlying synchronization data sharing mechanism as demonstrated by the two derived classes - `blocking_channel<T>` and `buffered_channel<T, int>`. Each of these classes has a specialized implementation of `_channel_sync_base<T>` which describes the actual mechanics of the channel. In the case of `blocking_channel<T>`, the `_channel_sync_base<T>` implementation has only a single pointer for internal storage of the data being passed through the channel. The `buffered_channel<T, int>`'s `_channel_sync_base<T>` implementation has a pre-allocated array that allows the implementation to buffer data inserted into the channel. This separation of API and internal mechanics was chosen to allow the decision to use a channel be orthogonal to the consumption of it being buffered or blocking.

    void producer(channel<int> c);

    // Limit the producer to the consumer's rate
    blocking_channel<int> block_ch;
    producer(block_ch);

    // Allow the producer to create a queue of data
    buffered_channel<int, 16> buff_ch;
    producer(buff_ch);


###V. Acknowledgements

The API surface of the proposed class is directly inspired by the current channel implementation found in the Go language. The reference implementation is inspired by work done by this proposal's author with [DisVM](http://www.vitanuova.com/inferno/papers/dis.html) which can be found in [Inferno OS](http://www.vitanuova.com/inferno/).


###VI. References

[1] Hoare, C. A. R. (1978). "Communicating sequential processes". Communications of the ACM. 21 (8): 666-677.

[2] The Go Programming Language. (n.d). The Go Programming Language Specification. Retrieved from [https://golang.org/ref/spec](https://golang.org/ref/spec).