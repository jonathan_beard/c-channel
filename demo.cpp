#include <iostream>
#include <thread>
#include "channels.h"

void producer(std::channel<int> &c) {
    for (auto i = 1; i <= 48; ++i) {
        c.insert(i);
        if (i % 16 == 0)
            c << -1;
    }

    c.close();
}

void consumer(std::channel<int> c) {
    std::cout << "++ channel - capacity(" << c.capacity() << ")\n";
    std::cout << "extraction\n";

    int r;
    c >> r;
    while (r != -1) {
        std::cout << r << "\n";
        c >> r;
    }

    std::cout << "iterator\n";
    for (auto iter = std::begin(c); iter->first != -1; ++iter) {
        std::cout << *iter << "\n";
    }

    std::cout << "range\n";
    for (auto r : c) {
        std::cout << r << "\n";
    }
}

void run(std::channel<int> &ch) {
    std::thread t1{ [&]() {
        auto p_c{ ch }; // Showcase explicit copy constructor
        producer(p_c);
    } };
    std::thread t2{ [&]() {
        consumer(ch); // Showcase implicit copy constructor
    } };

    t1.join();
    t2.join();
}

void sieve(const int this_prime, std::channel<int> &ch) {
    std::cout << this_prime << " ";

    int n;
    do {
        ch >> n;
        if (ch.is_closed())
            return;
    } while ((n % this_prime) == 0);

    std::blocking_channel<int> source_ch;
    std::thread t{ [&source_ch, n]()
    {
        sieve(n, source_ch);
    } };

    for (int m : ch)
    {
        if ((m % this_prime) != 0)
            source_ch << m;
    }

    source_ch.close();
    t.join();
}

// Based on the Limbo example (sieve-naive.b) found at http://www.gemusehaken.org/ipwl/sourcecode/book-examples/
void eratosthenes(int max_value) {
    int i = 2;
    std::blocking_channel<int> source_ch;
    std::thread t{ [&source_ch, i]() {
        sieve(i, source_ch);
    } };

    while (i <= max_value) {
        source_ch << (i++);
    }

    source_ch.close();
    t.join();
    std::cout << std::endl;
}

int main()
{
    // Example using blocking channel
    std::blocking_channel<int> ch_block;
    run(ch_block);

    // Example using non-blocking channel
    std::buffered_channel<int, 7> ch_buffer;
    run(ch_buffer);

    // Example of prime sieve example adopted from Limbo textbook
    eratosthenes(100);

    return 0;
}

